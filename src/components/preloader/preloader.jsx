import React from "react";
import './preloader.css';

function Preloader() {
   return(
      <div className="preloader">
         <div className="lds-dual-ring"/>
      </div>
   );
}


export default Preloader;