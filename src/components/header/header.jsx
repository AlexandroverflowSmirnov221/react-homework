import React from 'react';
import './header.css';


function Header({data, ownData}) {
   const date = new Date();         
   const datestring =
   ('0' + date.getDate()).slice(-2) + "."  +  
   ('0' + date.getMonth()+1).slice(-2) + "." + 
   date.getFullYear() + " " + 
   ('0' + date.getHours()).slice(-2) + ":" + 
   ('0' + date.getMinutes()).slice(-2) + ":" + 
   ('0' + date.getSeconds()).slice(-2);
   
   return(
      <header className='header'>
         <div className='header-title'>Is this chat?</div>
         <div className='header-users-count'>{data.length+1} participants</div>
         <div className='header-messages-count'>{ownData.length + data.length} messages</div>
         <div className='header-last-message-section'>
            <div className='header-last-message-text'>last message at</div>
            <div className='header-last-message-date'>{datestring}</div>
         </div>
      </header>
   );
}

export default Header;