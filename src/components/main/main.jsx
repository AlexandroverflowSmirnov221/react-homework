import React from 'react';
import './main.css';
import MessageInput from './message-input/messageInput';
import MessageList from './message-list/messageList';

function Main({data, ownData, edit, setEdit}) {
   return(
      <main className='main'>
         <MessageList  data={data} ownData={ownData} setEdit={setEdit}/>
         <MessageInput  ownData={ownData} edit={edit} setEdit={setEdit}/>
      </main>
   );
}

export default Main;