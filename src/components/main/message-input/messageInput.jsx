import React, {useState, useContext, useEffect } from 'react';
import './messageInput.css';
import Context from '../../../context';


function MessageInput({ownData, edit, setEdit}) {
   const [value, setValue] = useState('');
   const { addMessage } = useContext(Context);
   
   const updateMessage =(text, id, createdAt) => {
      const newMessage = ownData.map(ownData => 
         ownData.id === id ? {text, id, createdAt} : ownData);
      addMessage(newMessage);
      setEdit('');
   }

   useEffect(() => {
      if(edit) {
         setValue(edit.text);
      } else {
         setValue("")
      }
   }, [setValue, edit])

   function submitHandler(event) {
      event.preventDefault();
      if (!edit && value.trim()){
            addMessage(value);
            setValue('');
      } else {
         updateMessage(edit.id);
      }
   }

   return(
      <form className='message-input' onSubmit={submitHandler}>
        <input className='message-input-text' value={value} onChange={event => setValue(event.target.value)} 
         type='text' name='input' placeholder='Message'/>
        <button className='message-input-button' type='submit'>Send</button>
      </form>
   );
}

export default MessageInput;