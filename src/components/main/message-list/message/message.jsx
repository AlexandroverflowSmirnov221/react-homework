import React, {useContext} from 'react';
import './message.css';
import Context from '../../../../context';


function Message ({data}) {
   const classes = ['message-like', 'fa', 'fa-thumbs-up'];
   const { toggleLike } = useContext(Context);
   const date = new Date(data.createdAt);
   const datestring = ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2);

   if (data.editedAt) {
      classes.push('liked');
    } 

   return(
      <div className='message'>
         <div className="message-container">
            <div className='message-user-section'>
            <img className='message-user-avatar' src={data.avatar}>
            </img>
               <div className="message-user-text-section">
                  <div className="message-user-name-section">
                     <div className='message-user-name'>
                        {data.user}
                     </div>
                  <div className='message-time'>
                      {datestring}
                  </div>
                  </div>
                  <div className='message-text'>
                        {data.text}
                  </div>   
               </div>
            </div>
            <span aria-hidden="true" className={classes.join(' ')} onClick={()=>toggleLike(data.id)}>
            </span>
         </div>
      </div>
   );
}

export default Message;