import React from 'react';
import './messageList.css';
import Message from './message/message';
import OwnMessage from './own-message/ownMessage';

// function GetDate({data}) {
//    const compDate = new Date(data.createdAt);
//    const date = Date.now();
//    const diff = date.getTime() - compDate.getTime(); 
//    if (compDate.getTime() == date.getTime()) {
//       return "Today";
//    } else if (diff <= (24 * 60 * 60 *1000)) {
//       return "Yesterday";
//    } else { 
//       return compDate.toDateString(); 
//    }
// }

function MessageList ({data, ownData}) {
   const date = new Date();
   return(
      <div className='message-list'>
         { data.map(data => {
           return <Message data={data} key={data.id}/>
          }) }
      <div className='messages-divider'>
         <span className='messages-date'>{date.toDateString()}</span>
      </div>
          { ownData.map(ownData => {
            return <OwnMessage ownData={ownData} key={ownData.id}/>
           }) }
      </div>
   );
}

export default MessageList;