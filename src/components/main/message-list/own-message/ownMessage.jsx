import React, {useContext} from 'react';
import './ownMessage.css';
import Context from '../../../../context';

function OwnMessage ({ownData}) {
   const { removeMessage, editMessage } = useContext(Context);
   const date = new Date(ownData.createdAt);
   const datestring =  ('0' + date.getHours()).slice(-2) + ":" + ('0' + date.getMinutes()).slice(-2);

   return(
      <div className='own-message'>
         <div className="message-container">
            <div className='message-user-section'>
               <div className="message-user-text-section">
                  <div className="message-user-name-section">
               <div className='message-time'>   
                     {datestring}
               </div>
                  </div>
                  <div className='message-text'>  
                     {ownData.text}
                  </div>   
               </div>
            </div>
            <div className='message-edit-section'>
            <span className="message-edit fa fa-pencil-square-o" aria-hidden="true" onClick={() => editMessage(ownData.id)}></span>
            <span className="message-delete fa fa-trash" aria-hidden="true" onClick={() => removeMessage(ownData.id)}></span>
            </div>
         </div>
      </div>
   );
}

export default OwnMessage;